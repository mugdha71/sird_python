from matplotlib import pyplot as plt

import data_fit
from OnlineData import OnlineData
from SIRD import SIRD
from data_fit import get_fitted_data
from district_data import get_dhaka_info
from graph_dist import Graph
import numpy as np


if __name__ == '__main__':
    N = 53e6
    # N = 84000
    # lockdown 
    # onlineData = OnlineData(
    #     "https://raw.githubusercontent.com/datasets/covid-19/master/data/countries-aggregated.csv",
    #     'Bangladesh')
    # onlineData = OnlineData("local_data/countries-aggregated.csv", 'Bangladesh')

    # crop = 40
    # S = N - onlineData.confirmed
    # I = onlineData.active
    # R = onlineData.recovered
    # D = onlineData.deaths
    #
    # index_values = D.index.values[8:]
    # S = S.get(index_values)
    # I = I.get(index_values)
    # R = R.get(index_values)
    #
    # T = len(S)
    # N = 17e6
    # plt.plot(onlineData.recovered)
    # plt.show()
    # all country
    # x_fit_data, s_fit, i_fit, r_fit, d_fit = get_fitted_data(N, onlineData, start_date="2020-05-01",end_date="2020-06-10",show=True)
    # only dhaka
    N = 4.14e6
    x_fit_data, C_fit, s_fit, i_fit, r_fit, d_fit = get_dhaka_info()
    sird = SIRD(s_fit, i_fit, r_fit, d_fit, N)
    beta_t = []
    gamma_t = []
    mu_t = []
    r0_t = []

    for i in range(1, len(x_fit_data)):
        t = i
        t1 = i - 1
        beta_t.append(sird.get_beta(t, t1))
        mu_t.append(sird.get_mu(t, t1))
        gamma_t.append(sird.get_gamma(t, t1))
        r0_t.append(sird.get_r0(t, t1))

    # print(beta_t)
    avg_n = 5
    avg_beta = sum(beta_t[-avg_n:]) / avg_n
    # avg_mu = 0.0008677135456348333
    # avg_gamma = 0.014897364463428594
    avg_mu = sum(mu_t[-avg_n:]) / avg_n
    avg_gamma = sum(gamma_t[-avg_n:]) / avg_n

    # date = np.array(data_fit.string_starting_date, dtype=np.datetime64)
    date = np.array("2020-05-26", dtype=np.datetime64)
    date_list = []
    for i in x_fit_data:
        date_list.append(date+i)
    date_list = date_list[1:]

    title = "R_0"
    fig, ((ax1,ax2),(ax3,ax4)) = plt.subplots(2, 2)

    ax4.set_title(title)
    ax1.set_title("beta")
    ax2.set_title("mu")
    ax3.set_title("gamma")
    r0_t_croped = np.array(r0_t)
    r0_t_croped = r0_t_croped[~np.isnan(r0_t_croped)]
    print(r0_t_croped)
    print(np.std(r0_t_croped))
    print(np.mean(r0_t_croped))
    print("Date list size ", len(date_list))
    print("R0 list size ", len(beta_t))

    ax4.scatter(date_list[:], r0_t[:])

    ax1.scatter(date_list[:], beta_t)
    ax2.scatter(date_list[:], mu_t)
    ax3.scatter(date_list[:], gamma_t)

    fig.autofmt_xdate()
    fig.savefig("C:/Users/KME Hasan/PycharmProjects/SIRD_Model/" + title + ".png")
    fig.show()

    print("beta,mu,gamma")
    print(avg_beta,avg_mu,avg_gamma)
    sird.setConstant(avg_beta,avg_mu,avg_gamma)
    # sird.setConstant(beta_t[2], mu_t[2], gamma_t[2])

    init_value = len(s_fit) - avg_n
    # simulated data
    s, i, r, d, s_from_N = sird.generate_next_n_data(365 * 2, init_value)

    date = np.array(data_fit.string_starting_date, dtype=np.datetime64)
    date_list = date + np.arange(365 * 2+init_value)

    real_dates = date_list[:len(s_fit)]
    simulated_dates = date_list[init_value-1:]

    # print(get_std(s[:common_value], S[:-common_value]))

    graph = Graph()
    # graph.set_error(get_std(s[:common_value], S[:-common_value]),
    #                 get_std(i[:common_value], I[:-common_value]),
    #                 get_std(r[:common_value], R[:-common_value]),
    #                 get_std(d[:common_value], D[:-common_value]),
    #                 )
    graph.set_real_sird(real_sird=[s_fit, i_fit, r_fit, d_fit], real_dates=real_dates)
    graph.set_simulated_sird(simulated_sird=[s, i, r, d], simulated_dates=simulated_dates)
    # common_value = len(s_fit)
    # graph.set_real_sird(real_sird=[s_fit[-common_value:], i_fit[-common_value:], r_fit[-common_value:], d_fit[-common_value:]], real_dates=real_dates[-common_value:])
    # graph.set_simulated_sird(simulated_sird=[s[:common_value], i[:common_value], r[:common_value], d[:common_value]], simulated_dates=simulated_dates[:common_value])

    graph.draw()
