import datetime

from matplotlib import pyplot as plt


class Graph:
    def __init__(self, real_sird=None, real_dates=[], simulated_sird=None, simulated_dates=[]):
        """
        Constructor for graph class
        :param real_sird: contains list of real value of sird. ex. [s,i,r,d]
        :param real_dates: real dates as x axis value
        :param simulated_sird: contains list of simulated value of sird. ex. [s,i,r,d]
        :param simulated_dates: simulated dates as x axis value
        """
        if simulated_sird is None:
            simulated_sird = [[], [], [], []]
        if real_sird is None:
            real_sird = [[], [], [], []]
        self.real_s = real_sird[0]
        self.real_i = real_sird[1]
        self.real_r = real_sird[2]
        self.real_d = real_sird[3]

        self.simulated_s = simulated_sird[0]
        self.simulated_i = simulated_sird[1]
        self.simulated_r = simulated_sird[2]
        self.simulated_d = simulated_sird[3]

        self.real_dates = real_dates
        self.simulated_dates = simulated_dates

    def set_real_sird(self, real_sird, real_dates):
        """
        change or set real value of sird
        :param real_sird: contains list of real value of sird. ex. [s,i,r,d]
        :param real_dates: dates as x axis value
        :return:
        """
        self.real_s = real_sird[0]
        self.real_i = real_sird[1]
        self.real_r = real_sird[2]
        self.real_d = real_sird[3]
        self.real_dates = real_dates

    def set_simulated_sird(self, simulated_sird, simulated_dates):
        """
        change or set simulated value of sird
        :param simulated_sird: contains list of simulated value of sird. ex. [s,i,r,d]
        :param simulated_dates: simulated dates as x axis value
        :return:
        """
        self.simulated_s = simulated_sird[0]
        self.simulated_i = simulated_sird[1]
        self.simulated_r = simulated_sird[2]
        self.simulated_d = simulated_sird[3]
        self.simulated_dates = simulated_dates

    def set_error(self,s_err,i_err,r_err,d_err):
        self.s_err = s_err
        self.i_err = i_err
        self.r_err = r_err
        self.d_err = d_err

    def draw(self):
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
        ax1.set_title("susceptible people")
        ax1.semilogy(self.real_dates[:len(self.real_s)], self.real_s, label="Calculated data")
        # ax1.errorbar(self.simulated_dates, self.simulated_s, yerr=self.s_err, label="Simulated data")
        ax1.semilogy(self.simulated_dates, self.simulated_s, label="Simulated data")
        ax2.set_title("infected people")
        ax2.semilogy(self.real_dates[:len(self.real_i)], self.real_i, label="Calculated data")
        ax2.semilogy(self.simulated_dates, self.simulated_i, label="Simulated data")
        ax3.set_title("recovered people")
        ax3.semilogy(self.real_dates[:len(self.real_r)], self.real_r, label="Calculated data")
        ax3.semilogy(self.simulated_dates, self.simulated_r, label="Simulated data")
        ax4.set_title("deaths")
        ax4.semilogy(self.real_dates[:len(self.real_d)], self.real_d, label="Calculated data")
        ax4.semilogy(self.simulated_dates, self.simulated_d, label="Simulated data")

        ax1.set_xlim([datetime.date(2020, 5, 1), datetime.date(2020, 12, 31)])
        ax2.set_xlim([datetime.date(2020, 5, 1), datetime.date(2020, 12, 31)])
        ax3.set_xlim([datetime.date(2020, 5, 1), datetime.date(2020, 12, 31)])
        ax4.set_xlim([datetime.date(2020, 5, 1), datetime.date(2020, 12, 31)])

        ax1.legend()
        ax2.legend()
        ax3.legend()
        ax4.legend()
        fig.autofmt_xdate()
        plt.show()
        plt.clf()
        C = []
        C_real = []
        for i, r, d in zip(self.real_i, self.real_r, self.real_d):
            C_real.append(i + r + d)

        for i,r,d in zip(self.simulated_i,self.simulated_r,self.simulated_d):
            C.append(i+r+d)
        for i in range(1,len(self.simulated_dates)):
            print(self.simulated_dates[i],C[i]-C[i-1])
        plt.title("Confirmed case")
        plt.semilogy(self.real_dates, C_real, label="real data",marker='x')
        plt.semilogy(self.simulated_dates, C, label="Simulated data")
        plt.legend()
        plt.xlim([datetime.date(2020, 5, 1), datetime.date(2020, 12, 31)])
        plt.show()
