from datetime import datetime

import numpy as np
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt

from OnlineData import OnlineData

starting_date = datetime.strptime("2020-05-26", "%Y-%m-%d")
string_starting_date = "2020-05-26"

def gauss(x, A, mu, sigma):
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))
# def gaussian(x, mu, sig):
#     return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

# def func(x, a, b, c):
#     return a * np.exp(-b * x) + c


def days_between(d2):
    d2 = datetime.strptime(d2, "%Y-%m-%d")
    return abs((d2 - starting_date).days)


def fit(data, x_fit_data=None):
    xdata = []
    y_data = []
    x_date = data.index.values
    for i in data.index.values:
        d2 = str(i).split("T")[0]
        xdata.append(days_between(d2))
        y_data.append(data[i])
    try:
        (a, b, c), pcov = curve_fit(gauss, xdata, y_data)
    except:
        return xdata, y_data,xdata, y_data
    y_fit_data = []

    if x_fit_data is None:
        x_fit_data = []
        for i in range(xdata[0], xdata[-1] + 1):
            x_fit_data.append(i)

    for i in x_fit_data:
        y_fit_data.append(gauss(i, a, b, c))
    print(a, b, c)
    return xdata, y_data, x_fit_data, y_fit_data


def date_to_index(index, date):
    if date is None:
        return None
    for i in range(0, len(index)):
        if str(index[i]).split("T")[0] == date:
            return i
    return None


def get_fitted_data(N, online_data, start_date=None, end_date=None,show=None):
    C = online_data.confirmed[
        date_to_index(online_data.confirmed.index.values, start_date):
        date_to_index(online_data.confirmed.index.values, end_date)]

    I = online_data.active[
        date_to_index(online_data.active.index.values, start_date):
        date_to_index(online_data.active.index.values, end_date)]
    R = online_data.recovered[
        date_to_index(online_data.recovered.index.values, start_date):
        date_to_index(online_data.recovered.index.values, end_date)]
    D = online_data.deaths[
        date_to_index(online_data.deaths.index.values, start_date):
        date_to_index(online_data.deaths.index.values, end_date)]

    global starting_date, string_starting_date
    starting_date = datetime.strptime(str(C.index.values[0]).split("T")[0], "%Y-%m-%d")
    string_starting_date = str(C.index.values[0]).split("T")[0]
    x_data_c, c_data, x_fit_data, c_fit = fit(C)
    s_fit = list(N - np.array(c_fit))
    x_data_i, i_data, _, i_fit = fit(I, x_fit_data)
    x_data_r, r_data, _, r_fit = fit(R, x_fit_data)
    x_data_d, d_data, _, d_fit = fit(D, x_fit_data)
    if show is not None:
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
        ax1.set_title("Confirmed")
        ax2.set_title("active")
        ax3.set_title("recovered")
        ax4.set_title("death")
        ax1.scatter(x_data_c, c_data)
        ax1.plot(x_fit_data, c_fit)

        ax2.scatter(x_data_i, i_data)
        ax2.plot(x_fit_data, i_fit)

        ax3.scatter(x_data_r, r_data)
        ax3.plot(x_fit_data, r_fit)

        ax4.scatter(x_data_d, d_data)
        ax4.plot(x_fit_data, d_fit)
        fig.autofmt_xdate()
        fig.show()
        plt.show()
    return x_fit_data, s_fit, i_fit, r_fit, d_fit


if __name__ == '__main__':
    onlineData = OnlineData("local_data/countries-aggregated.csv", 'Bangladesh')

    N = 53e6
    x_fit_data, s_fit, i_fit, r_fit, d_fit = get_fitted_data(N, onlineData, start_date="2020-05-21",end_date="2020-06-10",show=True)
    # plt.plot(x_fit_data, i_fit)
    # plt.show()
    # C = onlineData.confirmed[20:40]
    # S = N - C
    # I = onlineData.active[20:]
    # R = onlineData.recovered[32:]
    # D = onlineData.deaths[30:]
    #
    #
    # x_date, y_data, yfit = fit(I)
    # title = "Data fitting for I_ig"
    # fig, ax = plt.subplots(1, 1)
    #
    # ax.set_title(title)
    # ax.plot(x_date, yfit, 'r-', label="fit")
    # ax.scatter(x_date, y_data, label="real")
    # ax.legend()
    # fig.autofmt_xdate()
    # fig.savefig("C:/Users/KME Hasan/PycharmProjects/SIRD_Model/" + title + ".png")
    # fig.show()
    # pass
