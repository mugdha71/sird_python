import pandas as pd
from matplotlib import pyplot as plt


def get_dhaka_data():
    csv = pd.read_csv("local_data/dhaka_confirm.csv")
    csv['Date'] = pd.to_datetime("2020-"+csv['Date'], format='%Y-%m-%d')  # Converts date strings to timestamp
    print(csv)

    plt.scatter(csv["Date"],csv["Confirmed"])
    plt.show()


if __name__ == '__main__':
    get_dhaka_data()
