import pandas as pd
import numpy as np
from matplotlib import pylab as plt
from scipy.optimize import curve_fit

from read_from_xlsx import get_dist_data


def func(x, A, mu, sigma):
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))
def get_dhaka_info():
    # file = "local_data/modified.csv"
    # df = pd.read_csv(file).dropna()  # Pandas function to read the .csv file
    # df['Date'] = pd.to_datetime(df['Date'], format='%Y-%m-%d')  # Converts date strings to timestamp
    # data = df
    data = get_dist_data()
    C = []
    N = 4.14e6
    s = 0
    st_value = 45
    print(data[st_value:])
    for i in data:
        s += i
        C.append(s)
    C = C[st_value:]
    mu = 0.0008677135456348333
    gamma = 0.014897364463428594
    S = [N]
    R = [0]
    D = [0]
    I = [C[0] - R[0] - D[0]]
    date = [0]
    C_fit = []
    S_fit = []
    (a, b, c), pcov = curve_fit(func, range(len(C)), C, maxfev=800)
    for i in  range(len(C)):
        C_fit.append(func(i, a, b, c))
        S_fit.append(N - C_fit[-1])


    for i in range(1,len(C_fit)):
        date.append(i+date[0])
        S.append(N-C_fit[i])
        R.append(R[-1] + I[-1] * gamma)
        D.append(D[-1] + I[-1] * mu)
        I.append(C_fit[i] - R[i] - D[i])
    plt.plot(C_fit)
    plt.plot(C)
    plt.show()
    return date,C_fit,S_fit,I,R,D
# print(data["Dhaka City"])

if __name__ == '__main__':
    get_dhaka_info()
    pass
