class SIRD:
    def __init__(self, s, i, r, d, N):
        self.s = s
        self.i = i
        self.r = r
        self.d = d
        self.N = N

        self.beta = None
        self.gamma = None
        self.mu = None
        self.r0 = None

    def get_beta(self, t, t1):
        self.beta = -self.N * (self.s[t] - self.s[t1]) / (self.s[t] * self.i[t])
        return self.beta

    def get_gamma(self, t, t1):
        self.gamma = (self.r[t] - self.r[t1]) / self.i[t]
        return self.gamma

    def get_mu(self, t, t1):
        self.mu = (self.d[t] - self.d[t1]) / self.i[t]
        return self.mu

    def get_r0(self, t=None, t1=None, beta=None, mu=None, gamma=None):
        if not beta:
            beta = self.get_beta(t, t1)
        if not mu:
            mu = self.get_mu(t, t1)
        if not gamma:
            gamma = self.get_gamma(t, t1)
        self.r0 = beta / (mu + gamma)
        return self.r0

    def setConstant(self,beta,mu,gamma):
        self.beta = beta
        self.mu = mu
        self.gamma = gamma

    def generate_next_n_data(self, n, t):
        s = [self.s[t]]
        s_from_N = [self.s[t]]
        i = [self.i[t]]
        r = [self.r[t]]
        d = [self.d[t]]
        for t in range(n):
            i_next = i[-1] + (self.beta * s[-1] * i[-1] / self.N - self.gamma * i[-1] - self.mu * i[-1])
            r_next = r[-1] + self.gamma * i[-1]
            d_next = d[-1] + self.mu * i[-1]
            s_next = s[-1] + (-1 * self.beta * s[-1] * i[-1] / self.N)

            i.append(i_next)
            r.append(r_next)
            d.append(d_next)
            s.append(s_next)

            s_from_N_next = self.N - i[-1] - r[-1] - d[-1]
            s_from_N.append(s_from_N_next)
        return s, i, r, d, s_from_N
