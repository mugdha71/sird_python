import pandas as pd
from numpy import NaN
from datetime import datetime


def get_dist_data(dist="Dhaka City"):
    xl_file = pd.ExcelFile("local_data/Pipilika_Coronavirus_cases.xlsx")
    # dfs = {sheet_name: xl_file.parse(sheet_name)
    #        for sheet_name in xl_file.sheet_names}
    sheet_1_data = xl_file.parse("Sheet1")
    indx = -1
    # find index of district
    for i in range(len(sheet_1_data["Dist"])):
        if sheet_1_data["Dist"][i] == dist:
            indx = i
            break
    indexs_all = sheet_1_data.loc[indx].index.values
    index_filter = []
    for i in indexs_all:
        # get only confirm value
        if type(i) == datetime: index_filter.append(i)

    last_pos = -1
    for i in range(len(index_filter) - 1, -1, -1):
        # remove last null value
        if sheet_1_data.loc[indx][index_filter[i]] is not NaN:
            last_pos = i
            break
    # using filter indexes get panda data and reverse it
    filter_data = sheet_1_data.loc[indx][index_filter[:last_pos]].iloc[::-1]
    filter_data = filter_data.dropna()
    # print(filter_data)
    return filter_data


if __name__ == '__main__':
    for i in get_dist_data():
        # if i is NaN:
        print(i)
    # print(get_dist_data())
